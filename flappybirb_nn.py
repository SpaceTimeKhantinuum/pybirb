import pygame
import neat
import time
import os
import random

pygame.font.init()

WIN_WIDTH = 500
WIN_HEIGHT = 800

GEN = 0

#BIRD_IMAGES_PATHS = [
#        'imgs/bird1.png',
#        'imgs/bird2.png',
#        'imgs/bird3.png'
#        ]

BIRD_IMAGES_PATHS = [
        'imgs/s1.png',
        'imgs/s2.png',
        'imgs/s3.png'
        ]

BIRD_IMGS = [ pygame.transform.scale2x( pygame.image.load( b ) ) for b in BIRD_IMAGES_PATHS ]
PIPE_IMG = pygame.transform.scale2x( pygame.image.load( 'imgs/pipe.png' ) )
BASE_IMG = pygame.transform.scale2x( pygame.image.load( 'imgs/base.png' ) )
BG_IMG = pygame.transform.scale2x( pygame.image.load( 'imgs/bg.png' ) )

STAT_FONT = pygame.font.SysFont("comicsans", 50)


class Bird(object):
    IMGS = BIRD_IMGS
    MAX_ROTATION = 25
    ROT_VEL = 20
    ANIMATION_TIME = 5

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.tilt = 0.
        self.tick_count = 0.
        self.vel = 0.
        self.height = self.y
        self.img_count = 0
        self.img = self.IMGS[0]

    def jump(self):
        self.vel = -10.5 # this goes up
        self.tick_count = 0 # to know when we last jumped
        self.height = self.y # where the bird jumped from

    def move(self):
        # what we call every single frame to move the bird
        self.tick_count += 1 # keep track of how many times we've moved since the last jump

        # displacement
        # how many pixels we are moving up or down this frame.
        # what we end up actually moving when we change the y position of the bird.
        d = self.vel*self.tick_count + 1.5*self.tick_count**2

        # terminal velocity
        if d >= 16:
            d = 16

        if d < 0:
            d -= 2

        self.y += d

        if d < 0 or self.y < self.height + 50:
            # moving upwards
            if self.tilt < self.MAX_ROTATION:
                self.tilt = self.MAX_ROTATION

        else:
            if self.tilt > -90:
                self.tilt -= self.ROT_VEL

    def draw(self, win):
        self.img_count += 1
       
       # cycle the image so the bird flaps
        if self.img_count < self.ANIMATION_TIME:
            self.img = self.IMGS[0]
        elif self.img_count < self.ANIMATION_TIME*2:
            self.img = self.IMGS[1]
        elif self.img_count < self.ANIMATION_TIME*3:
            self.img = self.IMGS[2]
        elif self.img_count < self.ANIMATION_TIME*4:
            self.img = self.IMGS[1]
        elif self.img_count == self.ANIMATION_TIME*4+1:
            self.img = self.IMGS[0]
            self.img_count = 0
        
        if self.tilt <= -80:
            self.img = self.IMGS[1]
            # don't flap when diving
            self.img_count = self.ANIMATION_TIME*2

        rotated_img = pygame.transform.rotate(self.img, self.tilt)
        topleft = (self.x, self.y)
        new_rect = rotated_img.get_rect(center=self.img.get_rect(topleft=topleft).center)
        win.blit(rotated_img, new_rect.topleft)

    def get_mask(self):
        """
        for collisions
        """
        return pygame.mask.from_surface(self.img)

class Pipe(object):
    GAP = 200
    VEL = 5

    def __init__(self, x):
        self.x = x
        self.height = 0

        # where the top and bottom pipe will be drawn
        self.top = 0
        self.bottom = 0
        self.PIPE_TOP = pygame.transform.flip(PIPE_IMG, False, True)
        self.PIPE_BOTTOM = PIPE_IMG

        # has the bird passed the pipe
        # for collisions purposes and AI.
        self.passed = False
        self.set_height()

    def set_height(self):
        # randomise pipe position
        self.height = random.randrange(50, 450)
        self.top = self.height - self.PIPE_TOP.get_height()
        self.bottom = self.height + self.GAP

    def move(self):
        self.x -= self.VEL

    def draw(self, win):
        win.blit(self.PIPE_TOP, (self.x, self.top))
        win.blit(self.PIPE_BOTTOM, (self.x, self.bottom))


    def collide(self, bird):
        bird_mask = bird.get_mask()
        top_mask = pygame.mask.from_surface(self.PIPE_TOP)
        bottom_mask = pygame.mask.from_surface(self.PIPE_BOTTOM)

        # calculate 'offset' i.e., how far apart are these masks from each other.
        top_offset = (self.x - bird.x, self.top - round(bird.y))
        bottom_offset = (self.x - bird.x, self.bottom - round(bird.y))

        # find point of overlap / collisions
        b_point = bird_mask.overlap(bottom_mask, bottom_offset)
        t_point = bird_mask.overlap(top_mask, top_offset)

        if t_point or b_point:
            return True
        
        return False

class Base(object):
    VEL = 5
    WIDTH = BASE_IMG.get_width()
    IMG = BASE_IMG

    def __init__(self, y):
        self.y = y
        self.x1 = 0
        self.x2 = self.WIDTH

    def move(self):
        self.x1 -= self.VEL
        self.x2 -= self.VEL

        # cycle two images to imitate a rolling background
        if self.x1 + self.WIDTH < 0:
            self.x1 = self.x2 + self.WIDTH

        if self.x2 + self.WIDTH < 0:
            self.x2 = self.x1 + self.WIDTH

    def draw(self, win):
        win.blit(self.IMG, (self.x1, self.y))
        win.blit(self.IMG, (self.x2, self.y))


def draw_window(win, birds, pipes, base, score, clock, gen):
    win.blit(BG_IMG, (0,0))
    

    for pipe in pipes:
        pipe.draw(win)

    base.draw(win)

    for bird in birds:
        bird.draw(win)

    text = STAT_FONT.render(f"Score: {score}", 1, (255,255,255))
    win.blit(text, (WIN_WIDTH - 10 - text.get_width(), 10))

    text = STAT_FONT.render(f"Gen: {gen}", 1, (255,255,255))
    win.blit(text, (10, 40))

    fps = STAT_FONT.render("FPS: "+str(int(clock.get_fps())), True, pygame.Color('white'))
    win.blit(fps, (10, 10))

    pygame.display.update()

def main(genomes, config):
    """
    our fitness function
    genomes, config for NEAT
    """
    global GEN
    GEN += 1
    nets = []
    ge = []
    birds = []

    for _, g in genomes:
        net = neat.nn.FeedForwardNetwork.create(g, config)
        nets.append(net)
        birds.append(Bird(230, 350))
        g.fitness = 0
        ge.append(g)

    base = Base(730)
    pipes = [Pipe(600)]
    win = pygame.display.set_mode((WIN_WIDTH, WIN_HEIGHT))
    clock = pygame.time.Clock()

    score = 0

    run = True
    while run:
        clock.tick(25)
        for event in pygame.event.get():
            if event.type == pygame.QUIT or event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                run = False
                print("quitting!")
                pygame.quit()
                quit()

        pipe_ind = 0
        if len(birds) > 0:
            if len(pipes) > 1 and birds[0].x > pipes[0].x + pipes[0].PIPE_TOP.get_width():
                pipe_ind = 1
        else:
            run = False
            break

        for x, bird in enumerate(birds):
            # pass values to NN and get it's output value
            # and if it's greater than 0.5 then jump
            bird.move()
            ge[x].fitness += 0.1

            output = nets[x].activate(
                    (
                        bird.y,
                        abs(bird.y - pipes[pipe_ind].height),
                        abs(bird.y - pipes[pipe_ind].bottom)
                        )
                    )

            if output[0] > 0.5:
                bird.jump()


        rem = [] # pipes to remove
        add_pipe = False
        for pipe in pipes:
            for x, bird in enumerate(birds):
                if pipe.collide(bird):
                    ge[x].fitness -= 1
                    birds.pop(x)
                    nets.pop(x)
                    ge.pop(x)

                # if pass a pipe generate a new one
                if not pipe.passed and pipe.x < bird.x:
                    pipe.passed = True
                    add_pipe = True

            if pipe.x + pipe.PIPE_TOP.get_width() < 0:
                # if pipe is off the screen then remove
                rem.append(pipe)

            pipe.move()

        if add_pipe:
            score += 1
            for g in ge:
                g.fitness += 5
            pipes.append(Pipe(600))
    
        for r in rem:
            pipes.remove(r)

        for x, bird in enumerate(birds):
            if bird.y + bird.img.get_height() >= 730 or bird.y < 0:
                birds.pop(x)
                nets.pop(x)
                ge.pop(x)

        base.move()
        draw_window(win, birds, pipes, base, score, clock, GEN)


def run(config_file):
    config = neat.config.Config(
            neat.DefaultGenome,
            neat.DefaultReproduction,
            neat.DefaultSpeciesSet,
            neat.DefaultStagnation,
            config_file
            )

    p = neat.Population(config)

    # stats reporters
    p.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)


    # out fittness function will be the main() function
    # and we will run it for 50 times
    winner = p.run(main, 50)


if __name__ == "__main__":
    local_dir = os.path.dirname(__file__)
    config_file = os.path.join(local_dir, "config-feedforward.txt")

    run(config_file)



