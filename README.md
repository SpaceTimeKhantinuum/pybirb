# pybirb

following the awesome tutorial series
https://github.com/techwithtim/NEAT-Flappy-Bird

Get the images from the above repo

## requirements

pygame, neat-python



## fix windowed frame rate issues

https://www.pygame.org/wiki/MacCompile#pygame%20with%20sdl2

the fix was to build with SDL2

```
brew install sdl2 sdl2_gfx sdl2_image sdl2_mixer sdl2_net sdl2_ttf
git clone https://github.com/pygame/pygame.git
cd pygame
python setup.py -config -auto -sdl2
python setup.py install
```


